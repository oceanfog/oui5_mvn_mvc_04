sap.ui.jsview("sapui5_simplestexam.secondView", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf sapui5_simplestexam.secondView
	*/ 
	getControllerName : function() {
		return "sapui5_simplestexam.secondView";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf sapui5_simplestexam.secondView
	*/ 
	createContent : function(oController) {
		
		return new sap.ui.commons.Button({text:"{/actionName2}",press:oController.doSomething});
	}

});
