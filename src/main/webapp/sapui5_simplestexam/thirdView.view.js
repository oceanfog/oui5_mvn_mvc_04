sap.ui.jsview("sapui5_simplestexam.thirdView", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf sapui5_simplestexam.thirdView
	*/ 
	getControllerName : function() {
		return "sapui5_simplestexam.thirdView";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf sapui5_simplestexam.thirdView
	*/ 
	createContent : function(oController) {
		alert("thirdView.view.js createContent");
		var oPanel = new sap.ui.commons.Panel();
		var oTable = new sap.ui.table.Table({editable:true});

		oTable.addColumn(new sap.ui.table.Column({
		  label: new sap.ui.commons.Label({text: "BusinessPartnerID"}),
		  template: new sap.ui.commons.TextView().bindProperty("text", "BusinessPartnerID"),
		  sortProperty: "carrid"
		}));

		oTable.addColumn(new sap.ui.table.Column({
		  label: new sap.ui.commons.Label({text: "EmailAddress"}),
		  template: new sap.ui.commons.TextField().bindProperty("value", "EmailAddress"),
		  sortProperty: "connid",
		}));
		 
		oTable.addColumn(new sap.ui.table.Column({
		  label: new sap.ui.commons.Label({text: "PhoneNumber"}),
		  template: new sap.ui.commons.TextField().bindProperty("value", "PhoneNumber"),
		  sortProperty: "fldate",
		}));

		oTable.addColumn(new sap.ui.table.Column({
		  label: new sap.ui.commons.Label({text: "CompanyName"}),
		  template: new sap.ui.commons.TextField().bindProperty("value", "CompanyName"),
		  sortProperty: "price",
		}));




		oTable.bindRows({path:"/BusinessPartnerCollection", parameters: {custom: {
		                           "BusinessPartnerID": '0100000001'
		                       }} });

		
		oPanel.addContent(oTable);
		oPanel.addContent(new sap.ui.commons.Button({text:'firstView',press:oController.doSomething}));
		
		oButton02 = new sap.ui.commons.Button({
			id : "id", // sap.ui.core.ID
			text : 'hello', // string
			enabled : true, // boolean
			visible : true, // boolean
			width : undefined, // sap.ui.core.CSSSize
			helpId : '', // string
			icon : '', // sap.ui.core.URI
			iconHovered : '', // sap.ui.core.URI
			iconSelected : '', // sap.ui.core.URI
			iconFirst : true, // boolean
			height : undefined, // sap.ui.core.CSSSize
			styled : true, // boolean
			lite : false, // boolean
			style : sap.ui.commons.ButtonStyle.Default, // sap.ui.commons.ButtonStyle
			tooltip : undefined, // sap.ui.core.TooltipBase
			customData : [ new sap.ui.core.CustomData({
				id : "id1", // sap.ui.core.ID
				key : undefined, // string
				value : undefined, // any
				writeToDom : false, // boolean, since 1.9.0
				tooltip : undefined, // sap.ui.core.TooltipBase
				customData : []
			// sap.ui.core.CustomData
			}) ], // sap.ui.core.CustomData
			ariaDescribedBy : [], // sap.ui.core.Control
			ariaLabelledBy : [], // sap.ui.core.Control
			press : [ function(oEvent) {
				var control = oEvent.getSource();
			}, this ]
		})
		
		oPanel.addContent(oButton02);
		
		return oPanel;
	}

});
