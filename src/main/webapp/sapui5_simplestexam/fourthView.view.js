sap.ui.jsview("sapui5_simplestexam.fourthView", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf sapui5_simplestexam.fourthView
	*/ 
	getControllerName : function() {
		return "sapui5_simplestexam.fourthView";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf sapui5_simplestexam.fourthView
	*/ 
	createContent : function(oController) {
		alert("fourthView.view.js createContent");
		var oPanel = new sap.ui.commons.Panel();
		var oTable = new sap.ui.table.Table({editable:true});

		oTable.addColumn(new sap.ui.table.Column({
		  label: new sap.ui.commons.Label({text: "BusinessPartnerID"}),
		  template: new sap.ui.commons.TextView().bindProperty("text", "BusinessPartnerID"),
		  sortProperty: "carrid"
		}));

		oTable.addColumn(new sap.ui.table.Column({
		  label: new sap.ui.commons.Label({text: "EmailAddress"}),
		  template: new sap.ui.commons.TextField().bindProperty("value", "EmailAddress"),
		  sortProperty: "connid",
		}));





		oTable.bindRows({path:"/BusinessPartnerCollection", parameters: {custom: {
		                           "BusinessPartnerID": '0100000001'
		                       }} });

		oPanel.addContent(oTable);
		
		return oPanel;
	}

});
