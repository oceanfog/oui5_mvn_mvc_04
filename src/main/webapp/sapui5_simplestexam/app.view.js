sap.ui.jsview("sapui5_simplestexam.app", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf sapui5_simplestexam.app
	*/ 
	getControllerName : function() {
		return "sapui5_simplestexam.app";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf sapui5_simplestexam.app
	*/ 
	createContent : function(oController) {
		alert("init app.view.js");
		
		// Buttons for switching Views
		oButton03 = new sap.ui.commons.Button({text:"Business Partner",press:function(){
			oPanel.removeAllContent();
			oPanel.addContent(oPanel_nav);
			oPanel.addContent(myView3);
		}});

		oButton04 = new sap.ui.commons.Button({text:"Show View 4",press:function(){
			oPanel.removeAllContent();
			oPanel.addContent(oPanel_nav);
			oPanel.addContent(myView4);
		}});

		oButton05 = new sap.ui.commons.Button({text:"gridLayout",press:function(){
			oPanel.removeAllContent();
			oPanel.addContent(oPanel_nav);
			oPanel.addContent(myGridLayoutView);
		}});
		oButton06 = new sap.ui.commons.Button({text:"SalesOrderItem",press:oController.moveMenu});
		
		var oPanel = new sap.ui.commons.Panel({text:"A Panel embedding one of two Views",content:myView3});
		var oPanel_nav = new sap.ui.commons.Panel({text:""});
		
		oPanel_nav.addContent(oButton03);
		oPanel_nav.addContent(oButton04);
		oPanel_nav.addContent(oButton05);
		oPanel_nav.addContent(oButton06);
		
		oPanel.addContent(oPanel_nav);
		
		return oPanel;
	}

});
