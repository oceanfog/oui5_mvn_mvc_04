sap.ui.jsview("sapui5_simplestexam.gridLayoutView", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf sapui5_simplestexam.gridLayoutView
	*/ 
	getControllerName : function() {
		return "sapui5_simplestexam.gridLayoutView";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf sapui5_simplestexam.gridLayoutView
	*/ 
	createContent : function(oController) {
		var oLayout1 = new sap.ui.layout.form.GridLayout("L1", {singleColumn: true});

        var oForm1 = new sap.ui.layout.form.Form("F1",{
                title: new sap.ui.core.Title({text: "Address Data", icon: "", tooltip: "Title tooltip"}),
                width: "60%",
                layout: oLayout1,
                formContainers: [
                        new sap.ui.layout.form.FormContainer("C1",{
                                formElements: [
                                        new sap.ui.layout.form.FormElement({
                                                label: new sap.ui.commons.Label({text: "Name", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                                                fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"})}),
                                                                new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"})})
                                                ]
                                        }),
                                        new sap.ui.layout.form.FormElement({
                                                label: new sap.ui.commons.Label({text: "Street / Number", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                                                fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"})}),
                                                                new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})})
                                                ]
                                        }),
                                        new sap.ui.layout.form.FormElement({
                                                label: new sap.ui.commons.Label({text: "Zip Code / City", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                                                fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                                                                new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"})})
                                                ]
                                        }),
                                        new sap.ui.layout.form.FormElement({
                                                label: new sap.ui.commons.Label({text: "Country", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                                                fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "auto"})})
                                                ]
                                        }),
                                        new sap.ui.layout.form.FormElement({
                                                label: new sap.ui.commons.Label({text: "Phone Number", layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                                                fields: [new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "1"})}),
                                                                new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                                                                new sap.ui.commons.TextField({layoutData: new sap.ui.layout.form.GridElementData({hCells: "2"})}),
                                                                new sap.ui.commons.Button({text:'firstView',press:oController.doSomething}),
                                                ]
                                        }),
                                ]
                        })
                ]
        });
        
        return oForm1;
	}

});
